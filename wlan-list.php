<?php
 $output = shell_exec('sudo iwlist wlan0 scan | grep ESSID');
 $response = array();

 foreach(explode('ESSID:', $output) as $ssid) {
     $clean_str = str_replace(array('"'), '', $ssid);
     $clean_str = trim($clean_str);
     
     if(!empty($clean_str)){
         array_push($response, $clean_str);
     }
 }
 echo json_encode($response);
?>