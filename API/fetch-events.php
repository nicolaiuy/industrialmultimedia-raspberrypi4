<?php
include "helpers.php";
include "db.php";

$endpoint = $URL . "api.php?media";
# $response = json_decode(callAPI("GET", $endpoint));

$response = shell_exec('curl -o /var/www/html/API/responseFile.txt https://www.nicolai.uy/api.php -w "%{response_code};%{time_total}" > /var/www/html/API/dataFile.txt 2> /var/www/html/API/informationFile.txt');

$response = json_decode(file_get_contents('/var/www/html/API/responseFile.txt'));

if($response){

    // Loop each cloud db events
    foreach ($response as $event) {
        $id = $event->id;

        $query = "SELECT * FROM proyecto.events WHERE id = " . $id;
        $result = $conn->query($query);
        $exists = $result->num_rows > 0;

        if (!$exists) {
            $title = $event->name;

            $responseMEDIA = shell_exec('curl -o /var/www/html/API/responseFileMedia.txt https://www.nicolai.uy/api.php?media -w "%{response_code};%{time_total}" > /var/www/html/API/dataFile.txt 2> /var/www/html/API/informationFile.txt');
            $responseMEDIA = json_decode(file_get_contents('/var/www/html/API/responseFileMedia.txt'))[0];


            $data = base64_decode($responseMEDIA->media);
            $date = new DateTime($event->start);
            $type = $event->type;
            
            $year = $date->format('y');
            $month = $date->format('m');
            $day = $date->format('d');
            $weekday = $date->format('w');
            $hour = $date->format('H');
            $minute = $date->format('i');
            $seconds = $date->format('i');

            $dateString = $date->format('Y-m-d H:i:s');
            echo $dateString;
            
            //Push to DB
            $sql = "INSERT INTO proyecto.events (id, title, data, date, type) VALUES (\"$id\", \"$title\", \"$data\", \"$dateString\", \"$type\")";
        
            if($conn->query($sql) === TRUE){

        
                // Create event Shell script
                $shell_env_variable = "export EVENTID=\"".$id."\"";
                $shell_event = "/usr/bin/python /var/www/html/GPIO/event.py";

                // HARDCODE eventos de este año
                // $shell_script = "if [ \"$(date \"+\%Y\")\" = " . $year. " ] ; then " . $shell_env_variable. " && " . $shell_event . "; fi";
                $shell_script = "if [ 21 = " . $year. " ] ; then " . $shell_env_variable. " && " . $shell_event . "; fi";
        
                // echo $shell_script;
                
                // Event Cron
                $cron_string = $minute . " " . $hour . " " . $day . " " . $month . " " . $weekday . " " . $shell_script;
                $cron_name = $year . $month . $day . $id;
                
                // Save Crone
                $add_cron = "crontab -l > mycron; echo \"" . $cron_string . "\" >> mycron; crontab mycron; rm mycron" ;
                shell_exec($add_cron);
        
            }else{
                echo $conn->error;
            }
        }else {
            echo "¡El hito ya está en la base!\n";
        }
    }
    $conn->close();
}
?>