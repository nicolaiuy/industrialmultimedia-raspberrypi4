<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <title>Ara | Conexión</title>
        <link rel="icon" href="assets/images/favicon.ico">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    </head>

    <script src="assets/js/jquery.min.js"></script>

    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col">
                    <div id="alerta" class="alert alert-light alert-dismissible fade" role="alert">
                        <span id="alerta-content">
                        </span>
                        <button id="close" type="button" class="close" data-hide="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>      

                    <img class="img-fluid mx-auto d-block mb-5"  src='../assets/images/logo-ingreso.svg' alt="Logo de Ara" width="135"
                    >
                    <p class="lead text-center mb-4">Ingresá los datos de acceso de tu red para conectar a Ara a la nube</p>


                    <form id="credentials" action="/" method="POST">
                        <div class="form-group">
                            <select class="form-control" name="ssid" id="ssid"></select>
                        </div>
                        <div class="form-group mb-4">
                            <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Contraseña">
                        </div>
                        <button id="submit" type="submit" class="btn btn-primary mx-auto d-block">Guardar</button>
                    </form>


                </div>
            </div>
        </div>  
    </body>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</html>