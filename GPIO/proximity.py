import RPi.GPIO as GPIO
import os
import time

GPIO.cleanup


GPIO.setmode(GPIO.BCM)

# VCC = 20
# GPIO.setup(VCC, GPIO.OUT)
# GPIO.output(VCC, GPIO.HIGH)

TRIG = 16
ECHO = 12

GPIO.setup(TRIG, GPIO.OUT)
GPIO.setup(ECHO, GPIO.IN)

GPIO.output(TRIG, True)
time.sleep(0.0001)
GPIO.output(TRIG, False)

while GPIO.input(ECHO) == False:
    start = time.time()

while GPIO.input(ECHO) == True:
    end = time.time()

sig_time = end-start

distance = sig_time / 0.000058

print(distance)

GPIO.cleanup
