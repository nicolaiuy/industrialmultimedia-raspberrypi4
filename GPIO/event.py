import RPi.GPIO as GPIO
import os
import time
import subprocess
import MySQLdb
import base64
from PIL import Image
import PIL.Image

conn = MySQLdb.connect(
    user='admin',
    passwd='admin123',
    host='127.0.0.1',
    database='proyecto'
)

# Get env var ID
eventid = os.environ["EVENTID"]

# Fetch DB content
cursor = conn.cursor()
cursor.execute("SELECT * FROM events WHERE ID=" + eventid)
event = None
for row in cursor.fetchall():
    event = row

event_title = event[1]
event_type = event[4]
data_format = event_type.split('/')[1]

path = '/home/pi/events-media/'

# Get BLOB from DB
cursor.execute("SELECT data FROM events WHERE ID=" + eventid)
data = cursor.fetchall()
data_content = data[0][0]

with open(path + '/event.' + data_format, 'wb') as f:
    f.write(data_content)

if(event_type == "video/mp4"):
    command = "omxplayer -o hdmi " + path + "/event." + data_format

if(event_type == "audio/mpeg"):
    command = "omxplayer -o local  " + path + "/event." + data_format

if(event_type == "image/jpeg"):
    command = "mirage -f  " + path + "/event." + data_format

# Turn notification led on    
GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.OUT)
GPIO.output(21, GPIO.HIGH)
time.sleep(5)

# Run message
os.system ("vcgencmd display_power 1 7")
os.system (command)
os.system ("vcgencmd display_power 0 7")

# Turn off led and end
GPIO.output(21, GPIO.LOW)
GPIO.cleanup()
