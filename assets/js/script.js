$(function () {
    const form = document.getElementById('credentials');
    const select = document.getElementById('ssid');
    const alerta = document.getElementById('alerta');
    const alertaCont = document.getElementById('alerta-content');

    // Get WLAN list
    $.ajax({
        url: "../../wlan-list.php",
        method: "GET",
        success: (response) => {
            let options = '';
            JSON.parse(response).forEach(option => {
                const optionElem = new Option(option, option);
                select.appendChild(optionElem);
            });;
        },
        error: (response) => {
            alert("Hubo un error cargando la lista, por favor refrescá el navegador.");
            console.log(response);
        },
        complete: () => {}
    });

    $("[data-hide]").on("click", function(){
        alerta.classList.add('hide');
        alerta.classList.remove('show');
    });

    form.addEventListener('submit', (event) => {

        event.preventDefault();

        // Form credentials
        const ssid = document.getElementById('ssid').value;
        const pwd = document.getElementById('pwd').value;

        let text = null;

        const data = {
            ssid,
            password: pwd,
        }

        // Check password value WPA2 compliance
        if (data.password.length < 6) {
            text = "La contraseña debe tener por lo menos 5 caracteres de largo";

            // Display alert
            alertaCont.innerHTML = text;
            alerta.classList.add('show');
            return;
        }

        // Ajax call to server check credentials
        let result;

        $.ajax({
            url: "../../creds.php",
            method: "POST",
            data,
            success: (response) => {
                result = response;
                console.log(result, response);
            },
            error: (response) => {
                alert("Hubo un error, ¡probá de nuevo!");
                console.log(response);
            },
            complete: () => {
                console.log(result);
                text = result ? "¡Ara se conectó con éxito!" : "Ups... la contraseña es incorrecta";

                // Display alert
                alertaCont.innerHTML = text;
                alerta.classList.add('show');
            }
        });

    });
});